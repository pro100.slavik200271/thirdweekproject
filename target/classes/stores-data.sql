use VKOLISNICHENKO;

create table Stores
(
    id      int         not null primary key auto_increment,
    address varchar(50) not null
);

# insert into Stores(id , address)
# VALUES (default, 'Kharkiv', 'Героїв Праці, 9-А'),
#        (default, 'Dnipro', 'Запорізьке шосе, 62-К'),
#        (default, 'Odessa', 'Вулиця Паустовського, 14'),
#        (default, 'Zaporozhye', 'Нікопольське шосе, 1Е,')