import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CreateTypes {

    CreateTypes() {
        insertTypes();
    }

    private Connection connect() throws SQLException {
        return DriverManager.getConnection("jdbc:mariadb://46.119.86.45:17306/VKOLISNICHENKO", "user", "resu");
    }

    private void insertTypes() {
        String SQL = "insert into Type(type) VALUES ('electrician'),\n" +
                "       ('plumbing'),\n" +
                "       ('food'), \n " +
                "('stationery'), \n" +
                "('tableware')";
        try (
                Connection conn = connect();
                PreparedStatement statement = conn.prepareStatement(SQL)) {
            statement.addBatch();
            statement.executeBatch();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
