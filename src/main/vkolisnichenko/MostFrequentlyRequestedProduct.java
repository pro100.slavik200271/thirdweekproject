import enums.Types;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;

public class MostFrequentlyRequestedProduct {
    private final Types currentType;
    private final Connection connection;

    MostFrequentlyRequestedProduct(Types currentType, Connection connection) {
        this.currentType = currentType;
        this.connection = connection;
    }

    public void getProductsWithCurrentType() throws SQLException {
        Statement stmt = connection.createStatement();
        String query = "SELECT * FROM Products WHERE type ='" + currentType.type + "'";
        ResultSet rs = stmt.executeQuery(query);
        ArrayList<String> storeAddress = new ArrayList<>();
        while (rs.next()) {
            storeAddress.add(rs.getString("address"));
        }
        Set<String> sh = new HashSet<>(storeAddress);
        final List<City> newList = sh.stream()
                .map(City::new)
                .collect(Collectors.toList());

        storeAddress.stream()
                .filter(i -> {
                    newList.stream().filter(j -> {
                        if (i.contains(j.getName())) {
                            j.setValue(j.getValue() + 1);
                        }
                        return false;
                    }).forEach(System.out::println);
                    return false;
                })
                .forEach(System.out::println);

        newList.sort(new Comparator<City>() {
            public int compare(City i1, City i2) {
                return (int) (i2.getValue() - i1.getValue());
            }
        });

        System.out.println(newList.stream().findFirst());

    }

}
