
import java.sql.*;
import java.util.*;

import enums.Types;

public class CreateProducts {
    private Connection connection;

    CreateProducts(Connection connection) {
        this.connection = connection;
        //insertStores();
        test();
    }

    private void insertStores() {

        try {
            PreparedStatement preparedStatement = null;
            Connection conn = connection;
            preparedStatement = conn.prepareStatement("INSERT INTO Products(type, name, address) VALUES (?, ?, ?)");

            List<String> types = getTypes("Type", "type");
            List<String> addressed = getTypes("Stores", "address");

            for (int i = 0; i < 1000; i++) {
                String randomType = getRandomChestItem(types);
                String randomAddress = getRandomChestItem(addressed);
                String product = null;
                if (Objects.equals(randomType, Types.ELECTRICIAN.type)) {
                    product = getRandomChestItem(electricianProducts());
                } else if (Objects.equals(randomType, Types.FOOD.type)) {
                    product = getRandomChestItem(foodProducts());
                } else if (Objects.equals(randomType, Types.PLUMBING.type)) {
                    product = getRandomChestItem(plumbingProducts());
                } else if (Objects.equals(randomType, Types.STATIONERY.type)) {
                    product = getRandomChestItem(stationeryProducts());
                } else if (Objects.equals(randomType, Types.TABLEWARE.type)) {
                    product = getRandomChestItem(tablewareProducts());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void test() {
        PreparedStatement preparedStatement = null;

        try {
            Connection conn = connection;
            preparedStatement = conn.prepareStatement("INSERT INTO Products(type, name, address) VALUES (?, ?, ?)");
            conn.setAutoCommit(false);

            List<String> types = getTypes("Type", "type");
            List<String> addressed = getTypes("Stores", "address");

            int batchTotal = 0;
            for (int i = 0; i < 10000; i++) {
                String randomType = getRandomChestItem(types);
                String randomAddress = getRandomChestItem(addressed);

                preparedStatement.setString(1, randomType);
                preparedStatement.setString(2, getProductName(randomType));
                preparedStatement.setString(3, randomAddress);
                preparedStatement.addBatch();
                if (batchTotal++ == 5000) {
                    preparedStatement.executeBatch();
                    preparedStatement.clearBatch();
                    batchTotal = 0;
                }
            }
            if (batchTotal > 0) {
                preparedStatement.executeBatch();
            }

            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    private String getProductName(String randomType) {
        if (Objects.equals(randomType, Types.ELECTRICIAN.type)) {
            return getRandomChestItem(electricianProducts());
        } else if (Objects.equals(randomType, Types.FOOD.type)) {
            return getRandomChestItem(foodProducts());
        } else if (Objects.equals(randomType, Types.PLUMBING.type)) {
            return getRandomChestItem(plumbingProducts());
        } else if (Objects.equals(randomType, Types.STATIONERY.type)) {
            return getRandomChestItem(stationeryProducts());
        } else if (Objects.equals(randomType, Types.TABLEWARE.type)) {
            return getRandomChestItem(tablewareProducts());
        }
        return getRandomChestItem(electricianProducts());
    }

    private List<String> getTypes(String tableName, String column) {
        try {
            Connection conn = connection;
            Statement stmt = conn.createStatement();
            String query = "SELECT * FROM " + tableName;
            ResultSet rs = stmt.executeQuery(query);
            ArrayList<String> types = new ArrayList<>();
            while (rs.next()) {
                types.add(rs.getString(column));
            }
            rs.close();
            return types;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }


    private ArrayList<String> electricianProducts() {
        ArrayList<String> electricianProducts = new ArrayList<>();
        electricianProducts.add("television");
        electricianProducts.add("notebook");
        electricianProducts.add("electric scooter");
        return electricianProducts;
    }

    private ArrayList<String> plumbingProducts() {
        ArrayList<String> plumbingProducts = new ArrayList<>();
        plumbingProducts.add("tap");
        plumbingProducts.add("pipe");
        plumbingProducts.add("shower");
        return plumbingProducts;
    }

    private ArrayList<String> foodProducts() {
        ArrayList<String> foodProducts = new ArrayList<>();
        foodProducts.add("milk");
        foodProducts.add("pizza");
        foodProducts.add("crisps");
        return foodProducts;
    }

    private ArrayList<String> stationeryProducts() {
        ArrayList<String> stationeryProducts = new ArrayList<>();
        stationeryProducts.add("pen");
        stationeryProducts.add("ruler");
        stationeryProducts.add("pencil");
        return stationeryProducts;
    }

    private ArrayList<String> tablewareProducts() {
        ArrayList<String> tablewareProducts = new ArrayList<>();
        tablewareProducts.add("a cup");
        tablewareProducts.add("plate");
        tablewareProducts.add("teapot");
        return tablewareProducts;
    }

    public String getRandomChestItem(List<String> items) {
        return items.get(new Random().nextInt(items.size()));
    }
}
