package enums;

public enum Types {
    ELECTRICIAN("electrician"), PLUMBING("plumbing"),
    FOOD("food"), STATIONERY("stationery"), TABLEWARE("tableware");

    public final String type;

    Types(String p) {
        type = p;
    }

    public String getPrice() {
        return type;
    }
}