import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CreateStores {
    private Connection connect() throws SQLException {
        return DriverManager.getConnection("jdbc:mariadb://46.119.86.45:17306/VKOLISNICHENKO", "user", "resu");
    }

    CreateStores() {
        insertStores();
    }

    private void insertStores() {

        String SQL = "insert into Stores(id , address) VALUES (default, 'Kharkiv, Героїв Праці, 9-А'),\n" +
                "       (default, 'Dnipro ,Запорізьке шосе, 62-К'),\n" +
                "       (default, 'Odessa ,Вулиця Паустовського, 14'),\n" +
                "       (default, 'Zaporozhye ,Нікопольське шосе, 1Е,')";
        try (
                Connection conn = connect();
                PreparedStatement statement = conn.prepareStatement(SQL)) {
            statement.addBatch();
            statement.executeBatch();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
