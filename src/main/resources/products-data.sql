use VKOLISNICHENKO;

create table Products
(
    id      int         not null primary key auto_increment,
    type    varchar(50) not null,
    name    varchar(50) not null,
    address varchar(50) not null
);
